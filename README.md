## Oku Beni
Bu, [QWorld](http://qworld.lu.lv/) tarafından hazırlanan Bronze eğitiminin QTurkey versiyonunun Türkçe tercümesidir. Orijinali (İngilizce) [şurada](https://gitlab.com/qkitchen/basics-of-quantum-computing) bulunur.

Bronze, [Dr. Abuzer Yakaryilmaz](http://abu.lu.lv) ([QLatvia](http://qworld.lu.lv/index.php/qlatvia/)) tarafından 2018 Ekim tarihinde yazılmıştır ve çoğu kısmı kendisi tarafından oluşturulmuştur. Dr. Maksims Dimitrijevs ([QLatvia](http://qworld.lu.lv/index.php/qlatvia/)) ve Dr. Özlem Salehi Köken ([QTurkey](http://qworld.lu.lv/index.php/qturkey/)) de Bronze projesine çalışma sayfaları yazarak ya da yazılanları kontrol ederek katkıda bulunmuşlardır.

## Kullanım

Yükleme bilgileri için [INSTALL.md](https://gitlab.com/ozlemsalehi/bronze_istanbul/blob/master/INSTALL.md) dosyasına bakınız.

## Lisans

Metin ve görseller Creative Commons Attribution 4.0 International Public License (CC-BY-4.0) lisansı ile sunulmuştur. Lisans ile ilgili detaylara şu adresten ulaşabilirsiniz: https://creativecommons.org/licenses/by/4.0/legalcode

Çalışma sayfalarındaki kod parçacıkları Apache License 2.0 lisansı ile lisanslanmıştır. Lisans ile ilgili detaylara şu adresten ulaşabilirsiniz: http://www.apache.org/licenses/LICENSE-2.0.

## Tercümeye Katkı Vermek
Hali hazırda tercüme süreci [Dr. Furkan Semih Dündar](https://fsemih.org) tarafından yapılmaktadır. Eğer siz de katkı vermek istiyorsanız özgeçmişinizi f.semih.dundar@yandex.com adresine kısa bir başvuru açıklamasıyla gönderiniz.
